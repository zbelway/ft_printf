/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 16:15:23 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 18:52:29 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdarg.h>
# include <stddef.h>
# include <wchar.h>
# include "../libft/includes/libft.h"

# define BLINKN "\e[5;0m"
# define BLINKR "\e[5;31m"
# define BLINKG "\e[5;32m"
# define BLINKY "\e[5;33m"
# define BLINKB "\e[5;34m"
# define BLINKM "\e[5;35m"
# define BLINKC "\e[5;36m"
# define BLINKW "\e[5;37m"
# define COLORN "\x1B[0m"
# define COLORR "\x1B[31m"
# define COLORG "\x1B[32m"
# define COLORY "\x1B[33m"
# define COLORB "\x1B[34m"
# define COLORM "\x1B[35m"
# define COLORC "\x1B[36m"
# define COLORW "\x1B[37m"

typedef	unsigned long long	t_uquad;
typedef	long long int		t_quad;
typedef struct		s_attribute
{
	size_t			written;
	int				n;
	int				fd;
	unsigned int	width;
	unsigned int	precision;
	char			left_adjustment;
	char			pad_zero;
	char			show_sign;
	char			begin_blank;
	char			pound;
	char			length;
	unsigned char	spec;
	char			neg;
	char			is_precision;
	char			p;
}					t_attribute;

int					ft_printf(const char *format, ...);
void				pr_str(va_list args, t_attribute *att);
void				pr_int(va_list args, t_attribute *att);
void				parse_spec(const char **format, t_attribute *att);
void				parse_flags(const char **s, t_attribute *att);
void				parse_width(const char **s, va_list args,
		t_attribute *att);
void				parse_precision(const char **s, va_list args,
		t_attribute *att, int n);
void				parse_length(const char **s, t_attribute *att);
void				put_num(intmax_t n, t_attribute *att);
void				fill_space(t_attribute *att, unsigned int n);
void				pr_char(va_list args, t_attribute *att);
void				pr_percent(t_attribute *att, unsigned char c);
void				pr_hex(va_list args, t_attribute *att);
void				pr_written(va_list args, t_attribute *att);
void				pr_float(va_list args, t_attribute *att);
char				*ft_itoa_base_mod(intmax_t num, t_attribute *att);
char				*ft_uitoa_base_mod(uintmax_t num, t_attribute *att);
intmax_t			unsigned_int(va_list args, t_attribute *att);
void				iter_intmax(intmax_t n);
int					parse_color(const char **s, t_attribute *att);
void				pr_pointer(va_list args, t_attribute *att);
unsigned int		my_putnwstr(wchar_t *s, size_t len, int fd, int p);
char				*my_strcpy(char *dup, char *str);
void				combine_num_parts(char *s1, char *s2, t_attribute *att);
void				pr_gloat(va_list args, t_attribute *att);
void				pr_eloat(va_list args, t_attribute *att);
void				put_float(char *s, t_attribute *att);

#endif
