/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 18:03:31 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 18:00:10 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				parse_length(const char **s, t_attribute *att)
{
	parse_flags(s, att);
	while (**s == 'h' || **s == 'l' || **s == 'j' || **s == 'z' || **s == 't'
			|| **s == 'q' || **s == 'L')
	{
		if (!att->length && (**s == 'j' || **s == 'z' || **s == 't'
					|| **s == 'q' || **s == 'L'))
			att->length = **s;
		if (!att->length && ((*s[0] == 'h') || (*s[0] == 'l')))
		{
			att->length = **s;
			(*s)++;
			if ((*s[0] == 'h' || *s[0] == 'l'))
				att->length = (**s - ('a' - 'A'));
		}
		else
			(*s)++;
	}
}

void				parse_precision(const char **s, va_list args,
		t_attribute *att, int n)
{
	while (**s == '.')
	{
		n = 0;
		att->p = 0;
		att->is_precision = **s;
		(*s)++;
		while (ft_isdigit(**s) || **s == '*')
		{
			if (**s == '*')
				n = (unsigned)va_arg(args, int);
			else
			{
				n *= 10;
				n += (**s - '0');
				att->precision = n;
			}
			(*s)++;
		}
		if (n >= 1)
			att->p = '.';
	}
	if (n >= 0)
		att->precision = n;
	else
		att->is_precision = 0;
}

void				parse_width(const char **s, va_list args, t_attribute *att)
{
	unsigned int	n;
	int				m;

	m = 0;
	n = 0;
	while (ft_isdigit(**s) || **s == '*')
	{
		if (**s == '*')
		{
			m = (va_arg(args, int));
			if (m < 0)
				att->left_adjustment = '-';
			att->width = ft_absval(m);
		}
		else
		{
			n *= 10;
			n += (**s - '0');
			att->width = n;
		}
		(*s)++;
	}
	parse_flags(s, att);
}

void				parse_flags(const char **s, t_attribute *att)
{
	while (**s == '#' || **s == '-' || **s == '+' || **s == ' ' || **s == '0')
	{
		if (**s == '+')
		{
			att->show_sign = '1';
			att->begin_blank = 0;
		}
		else if (**s == ' ' && !att->show_sign)
			att->begin_blank = '1';
		else if (**s == '-')
		{
			att->left_adjustment = '1';
			att->pad_zero = 0;
		}
		else if (**s == '0' && !att->left_adjustment)
			att->pad_zero = '1';
		else if (**s == '#')
			att->pound = '1';
		(*s)++;
	}
}

void				parse_spec(const char **s, t_attribute *att)
{
	parse_flags(s, att);
	if (**s == 's' || **s == 'S' || **s == 'p' || **s == 'd' || **s == 'D'
			|| **s == 'i' || **s == 'o' || **s == 'O' || **s == 'u'
			|| **s == 'U' || **s == 'x' || **s == 'X' || **s == 'c'
			|| **s == 'C' || **s == 'n' || **s == '%' || **s == 'f'
			|| **s == 'F' || **s == 'b' || **s == '!' || **s == 'B'
			|| **s == 'e' || **s == 'E' || **s == 'g' || **s == 'G')
	{
		att->spec = **s;
		(*s)++;
	}
	else if (**s >= 'A' && **s <= 'Z')
	{
		pr_percent(att, **s);
		(*s)++;
	}
	else
		att->spec = '\0';
}
