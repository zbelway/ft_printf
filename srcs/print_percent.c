/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_percent.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/09 15:05:18 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/20 19:40:39 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				pr_percent(t_attribute *att, unsigned char c)
{
	unsigned int	i;

	i = 0;
	if (att->width && att->precision && att->precision < att->width)
		i = att->width - att->precision;
	else if (att->width && att->precision && att->precision >= att->width)
		i = 0;
	else if (att->width)
		i = att->width - 1;
	if (att->left_adjustment)
	{
		write(att->fd, &c, 1);
		att->written++;
		fill_space(att, i);
	}
	else
	{
		fill_space(att, i);
		write(att->fd, &c, 1);
		att->written++;
	}
}
