/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_color.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/17 13:45:48 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 18:50:54 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		color_collection(const char s, t_attribute *att)
{
	if (s == 'N')
		ft_putstr_fd(COLORN, att->fd);
	else if (s == 'R')
		ft_putstr_fd(COLORR, att->fd);
	else if (s == 'G')
		ft_putstr_fd(COLORG, att->fd);
	else if (s == 'Y')
		ft_putstr_fd(COLORY, att->fd);
	else if (s == 'B')
		ft_putstr_fd(COLORB, att->fd);
	else if (s == 'M')
		ft_putstr_fd(COLORM, att->fd);
	else if (s == 'W')
		ft_putstr_fd(COLORW, att->fd);
	else if (s == 'C')
		ft_putstr_fd(COLORC, att->fd);
}

void		star_collection(const char s, t_attribute *att)
{
	if (s == 'N')
		ft_putstr_fd(BLINKN, att->fd);
	else if (s == 'R')
		ft_putstr_fd(BLINKR, att->fd);
	else if (s == 'G')
		ft_putstr_fd(BLINKG, att->fd);
	else if (s == 'Y')
		ft_putstr_fd(BLINKY, att->fd);
	else if (s == 'B')
		ft_putstr_fd(BLINKB, att->fd);
	else if (s == 'M')
		ft_putstr_fd(BLINKM, att->fd);
	else if (s == 'W')
		ft_putstr_fd(BLINKW, att->fd);
	else if (s == 'C')
		ft_putstr_fd(BLINKC, att->fd);
}

int			put_star(const char **s, t_attribute *att)
{
	char	c;

	if (**s == 'N' || **s == 'R' || **s == 'G' || **s == 'B' || **s == 'Y'
			|| **s == 'M' || **s == 'C' || **s == 'W')
	{
		c = **s;
		(*s)++;
		if (**s == '}')
		{
			(*s)++;
			star_collection(c, att);
			return (1);
		}
		else
		{
			(*s)--;
			return (0);
		}
	}
	(*s)--;
	return (0);
}

int			put_color(const char **s, t_attribute *att)
{
	char	c;

	if (**s == 'N' || **s == 'R' || **s == 'G' || **s == 'B' || **s == 'Y'
			|| **s == 'M' || **s == 'C' || **s == 'W')
	{
		c = **s;
		(*s)++;
		if (**s == '}')
		{
			(*s)++;
			color_collection(c, att);
			return (1);
		}
		else
		{
			(*s)--;
			return (0);
		}
	}
	(*s)--;
	return (0);
}

int			parse_color(const char **s, t_attribute *att)
{
	(*s)++;
	if (**s == '*')
	{
		(*s)++;
		if (put_star(s, att))
			return (1);
		else
			(*s)--;
		return (0);
	}
	else if (put_color(s, att))
		return (1);
	(*s)--;
	return (0);
}
