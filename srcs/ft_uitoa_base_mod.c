/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uitoa_base_mod.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 22:08:15 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 18:37:23 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#define BASEX "0123456789ABCDEF"
#define BASE "0123456789abcdef"

static char			*get_num(uintmax_t num, int base, t_attribute *att)
{
	char			*s;
	unsigned int	i;

	s = ft_strnew(64);
	i = 0;
	while (num > 0)
	{
		if (att->spec == 'x' || att->spec == 'o')
			s[i++] = BASE[num % base];
		else if (att->spec == 'X' || att->spec == 'O')
			s[i++] = BASEX[num % base];
		else
			s[i++] = BASE[num % base];
		num /= base;
	}
	while (i < att->precision)
		s[i++] = '0';
	s[i] = '\0';
	ft_strrev(s);
	return (s);
}

char				*ft_uitoa_base_mod(uintmax_t num, t_attribute *att)
{
	char			*s;

	if (num == 0)
	{
		s = ft_strnew(2 + att->precision);
		s[0] = '0';
		if (att->precision > 0)
			s[att->precision] = '\0';
		else
			s[1] = '\0';
		while (att->precision > 1)
			s[--att->precision] = '0';
		return (s);
	}
	else if (att->spec == 'o' || att->spec == 'O')
		return (get_num(num, 8, att));
	else if (att->spec == 'x' || att->spec == 'X' || att->spec == 'p')
		return (get_num(num, 16, att));
	else if (att->spec == 'b' || att->spec == 'B')
		return (get_num(num, 2, att));
	else
		return (get_num(num, 10, att));
}
