/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_float.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/20 21:19:54 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/22 18:51:02 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				put_float_str(char *s, t_attribute *att, unsigned int i)
{
	if ((att->left_adjustment || att->pad_zero) && att->show_sign && !att->neg)
		ft_putchar_fd('+', att->fd);
	else if (!att->neg && att->begin_blank)
		if ((att->left_adjustment || att->pad_zero) && i--)
			ft_putchar_fd(' ', att->fd);
	if (att->pad_zero && att->neg)
		ft_putchar_fd('-', att->fd);
	if (!att->left_adjustment)
		fill_space(att, i);
	if (!att->neg && att->show_sign && !att->left_adjustment && !att->pad_zero)
		ft_putchar_fd('+', att->fd);
	if (!att->pad_zero && att->neg)
		ft_putchar_fd('-', att->fd);
	ft_putstr_fd(s, att->fd);
	if (att->left_adjustment)
		fill_space(att, i);
	ft_strdel(&s);
}

void				put_float(char *s, t_attribute *att)
{
	unsigned int	len;

	len = ft_strlen(s);
	if (att->neg)
		len++;
	else if (att->show_sign)
		len++;
	if (len >= att->width)
	{
		put_float_str(s, att, 0);
		att->written += len;
	}
	else
	{
		len = att->width - len;
		put_float_str(s, att, len);
		att->written += att->width;
	}
}

void				combine_num_parts(char *s1, char *s2, t_attribute *att)
{
	char			*str;
	char			*cpy;
	int				n;

	if (att->spec == 'g' || att->spec == 'G')
		n = att->precision;
	else
		n = 100;
	str = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + 1);
	if (!str)
		return ;
	cpy = str;
	while (*s1 && n--)
		*str++ = *s1++;
	if (att->precision)
		*str++ = '.';
	while (*s2 && n--)
		*str++ = *s2++;
	put_float(cpy, att);
}

void				convert_float(long long i, long double d,
		int p, t_attribute *att)
{
	long long int	n;
	char			*s1;
	char			*s2;

	n = 1;
	att->precision = p;
	s1 = ft_itoa(i);
	if (p == 0)
	{
		s2 = ft_strnew(1);
		s2[0] = '.';
		s2[1] = '\0';
	}
	else
	{
		s2 = ft_strnew(p);
		s2[p] = '\0';
		while (p-- > 0)
			n *= 10;
		n = (n * d + .5);
		s2 = my_strcpy(s2, ft_itoa(n));
	}
	combine_num_parts(s1, s2, att);
	free(s1);
	free(s2);
}

void				pr_float(va_list args, t_attribute *att)
{
	long double		d;
	long long		i;

	if (att->length == 'L')
		d = va_arg(args, long double);
	else
		d = va_arg(args, double);
	if (d < 0)
	{
		att->neg = 1;
		d *= -1;
	}
	i = d;
	d -= i;
	if (att->is_precision && att->precision == 0 && !att->pound)
		put_num(i, att);
	else if (att->is_precision && att->precision == 0 && att->pound)
		convert_float(i, d, 0, att);
	else if (!att->is_precision)
		convert_float(i, d, 6, att);
	else
		convert_float(i, d, att->precision, att);
}
