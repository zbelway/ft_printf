/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/15 18:31:59 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/21 23:26:17 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void	*ft_memalloc(size_t size)
{
	void	*s;

	if (!size)
		return (NULL);
	s = (void *)malloc(size);
	if (!s)
		return (NULL);
	ft_memset(s, 0, size);
	return (s);
}
