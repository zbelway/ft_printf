/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 16:03:38 by zbelway           #+#    #+#             */
/*   Updated: 2016/03/20 19:37:35 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void		ft_putnstr_fd(const char *s, size_t len, int fd)
{
	size_t	i;

	i = 0;
	while (s[i] && i < len)
		ft_putchar_fd(s[i++], fd);
}
