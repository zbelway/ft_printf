/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 19:52:20 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/22 17:59:24 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void		*ft_memchr(const void *str, int c, size_t n)
{
	size_t	i;
	void	*tmp;

	i = 0;
	tmp = (void *)str;
	while (i < n)
	{
		if (*(unsigned char *)tmp == (unsigned char)c)
			return (tmp);
		tmp = (char *)tmp + 1;
		i++;
	}
	return (NULL);
}
