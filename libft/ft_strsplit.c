/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 19:42:14 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/24 14:47:38 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

static int	count_words(char *s, char c)
{
	int n;

	n = 0;
	while (*s)
	{
		if (*s != c)
		{
			n++;
			while (*s && *s != c)
				s++;
		}
		if (*s)
			s++;
	}
	return (n);
}

static void	input_word(char *place, char **word, char c)
{
	*place = *place;
	while (**word && **word != c)
	{
		*place++ = **word;
		*word += 1;
	}
	*place = '\0';
}

static int	word_len(char *s, char c)
{
	int i;

	i = 0;
	while (s[i] && s[i] != c)
		i++;
	return (i);
}

char		**ft_strsplit(char *s, char c)
{
	int		i;
	char	**table;
	char	**tmp;

	if (!s)
		return (NULL);
	i = count_words(s, c);
	if (!(table = (char **)malloc(sizeof(char *) * (i + 1))))
		return (NULL);
	table[i] = 0;
	tmp = table;
	while (*s)
	{
		if (*s != c)
		{
			i = word_len(s, c);
			*table = (char *)malloc(sizeof(char) * i);
			input_word(*table, &s, c);
			table++;
		}
		if (*s)
			s++;
	}
	return (tmp);
}
