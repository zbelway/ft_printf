NAME = libftprintf.a

SOURCES = srcs/parse.c \
	srcs/ft_printf.c \
	srcs/print_str.c \
	srcs/print_int.c \
	srcs/put_num.c \
	srcs/print_char.c \
	srcs/print_percent.c \
	srcs/ft_uitoa_base_mod.c \
	srcs/parse_color.c \
	srcs/print_hex.c \
	srcs/print_float.c \
	srcs/my_putnwstr.c \
	srcs/my_strcpy.c \
	srcs/print_gloat.c \
	libft/ft_strchr.c \
	libft/ft_putstr.c \
	libft/ft_putchar.c \
	libft/ft_putnstr.c \
	libft/ft_strlen.c \
	libft/ft_itoa.c \
	libft/ft_atoi.c \
	libft/ft_putnbr.c \
	libft/ft_putnbr_fd.c \
	libft/ft_strdup.c \
	libft/ft_putchar_fd.c \
	libft/ft_putstr_fd.c \
	libft/ft_strcpy.c \
	libft/ft_bzero.c \
	libft/ft_strdel.c \
	libft/ft_isdigit.c \
	libft/ft_strrev.c \
	libft/ft_strnew.c \
	libft/ft_memset.c \
	libft/ft_putwstr.c \
	libft/ft_wstrlen.c \
	libft/ft_putwchar.c \
	libft/ft_absval.c \
	libft/ft_putnstr_fd.c \
	libft/ft_putwstr_fd.c \
	libft/ft_putwchar_fd.c \
	libft/ft_strcat.c

OBJECTS = $(subst .c,.o,$(SOURCES))

FLAGS = -Wall -Werror -Wextra

CC = gcc

all: $(NAME)

$(NAME): $(OBJECTS)
	ar rc $(NAME) $(OBJECTS)
	ranlib $(NAME)

%.o: %.c
	$(CC) $(FLAGS) -c -o $@ $^ -I ./includes/

clean:
	rm -rf $(OBJECTS)

fclean: clean
	rm -rf $(NAME)

re: fclean all clean

.PHONY: re fclean clean all
